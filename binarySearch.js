let array = [1, 3, 4, 6, 8, 9, 11, 12, 15, 17, 18, 19];

function binarySearch(arr, target) {
    let startIdx = arr[0];
    let endIdx = arr.length - 1;

    while (startIdx <= endIdx) {
        let middleIdx = Math.floor((endIdx + startIdx) / 2);
        if (target === arr[middleIdx]) {
            return middleIdx;
        } else if (target < arr[middleIdx]) {
            endIdx = middleIdx - 1;
        } else {
            startIdx = middleIdx + 1;
        }
    }
    return -1;
}
console.log(binarySearch(array, 9));


// Реализовать бинарный поиск в отсортированном массиве. Функция binarySearch 
// принимает отсортированный массив arr и целевое значение target, и возвращает 
// индекс элемента target в массиве arr, если он найден, иначе возвращает -1.