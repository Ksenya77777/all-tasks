function flatArr(arr) {
  let result = [];
  for (let el of arr) {
    if (typeof el === 'object') {
      result = result.concat(flatArr(el));
    } else {
      result.push(el)
    }
  }
  return result;
}

let array = [1, 2, 3, [4, 5], [6, 7, [8, 9, [10]]]]

console.log(flatArr(array));


// Написать функцию FlatArr, которая принимает массив arr с вложенными массивами
//  и возвращает новый массив, содержащий все элементы вложенных массивов в одном 
//  уровне, без вложенности.