let arr = [1, 4, 6, 9, 66, 11, 3, 2, 44, 33]

function bubbleSortConcept(arr) {
  let result = [...arr]

  for (let i = 0; i < result.length; i++) {

    for (let j = 0; j < result.length - 1; j++) {
      
      if (result[j] > result[j + 1]) {
        let buffer = result[j]
        result[j] = result[j + 1]
        result[j + 1] = buffer
      }

    }

  }
  return result;
}
console.log(bubbleSortConcept(arr));