function factorial(n) {
  if (n == 1) {
      return n;
  }
  return n * factorial(n - 1);
}
console.log(factorial(5));


// написать функцию factorial(n), которая возвращает n!, используя рекурсию.

// alert( factorial(5) ); // 120
// P.S. Подсказка: n! можно записать как n * (n-1)! Например: 3! = 3*2! = 3*2*1! = 6