// Есть объект(любой) нужно создать его независимую от оригинала копию

let user = {
  name:'Ivan',
  age:26
}

let person = {...user}
let person2 = Object.assign({},user)
let person3 = JSON.parse(JSON.stringify(user));


// Необходимо создать функцию, объект и класс с секретным полем (password) которое можно получить, но нельзя изменить

function create() {
  let password = '1234';

  return function(){
    return password;
  }

}

const secretObject = create();
console.log(secretObject()); 


// Есть строка, необходимо посчитать сколько раз встречается каждый символ 
// строки ( “длинношеее” -> н-2, е-3, ш-1 и тд)

function seach(str) {
  let word = {}

  for (let i = 0; i < str.length; i++) {
    const el = str[i];
    if (word[el]) {
      word[el] += 1
    } else {
      word[el] = 1
    }
  }
  return word
}
console.log(seach('длинношеее'));

