function factorialIsNotRec(num) {
  let value = 1;
  for (let i = 2; i <= num; i++) {
   value = value * i
  }
   return value;
}

console.log(factorialIsNotRec(5));


// Условие задачи "Факториал без использования рекурсии".

// Функция должна решать задачу без использования рекурсии, используя цикл.

